import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
  CreateEventComponent,
  CreateSessionComponent,
  EventDetailsComponent,
  EventListResolverService,
  EventResolverService,
  EventsListComponent,
} from './events/index';

import { Error404Component } from './errors/error-404.component';


const routes : Routes = [
  { path: 'events', component: EventsListComponent, resolve: { events: EventListResolverService }},
  { path: 'events/new', component: CreateEventComponent, canDeactivate: [ 'canDeactiateCreateEvent' ]},
  { path: 'events/:id', component: EventDetailsComponent, resolve: { event: EventResolverService }},
  { path: 'events/session/new', component: CreateSessionComponent },

  { path: '404', component: Error404Component},
  { path: '', redirectTo: 'events', pathMatch: 'full'},

  { path: 'user', loadChildren: () => import('./user/user.module').then(m=>m.UserModule)}
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class EventRoutingModule { }
