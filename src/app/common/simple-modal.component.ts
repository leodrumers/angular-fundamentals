import { Component, ElementRef, Input, ViewChild, Inject } from '@angular/core';
import { JQ_TOKEN } from './jQuery.service';

@Component({
    selector: 'simple-modal',
    templateUrl: './simple-modal.component.html',
    styles: [`
        html, body {
            background-color: #cb2322;
        }
        .modal-body { height: 250px; overflow-y: scroll;}
        .modal-content {
            background-color: #4f5e6e;
        }

    `]
})
export class SimpleModalComponent {
    @Input() title: string;
    @Input() elementId : string;

    @ViewChild('modalContainer') containerEl: ElementRef;

    constructor(@Inject(JQ_TOKEN) private $:any){

    }

    closeModal() {
        this.$(this.containerEl.nativeElement).hide();
    }
}