import { Component, Input } from '@angular/core'

import { IEvent } from './shared/index'

@Component({
  selector: 'event-thumbnail',
  template: `
    <div [routerLink]="['/events', event.id]" class="well hoverwell thumbnail mb-3 p-3">
      <h2 mb-4>{{event?.name | uppercase}}</h2>
      <div><strong>{{event?.date | date:'medium'}}</strong></div>
      <div [ngStyle]="getStartTimeStyle()" [ngSwitch]="event?.time"><strong>{{event?.time}} </strong>
        <span *ngSwitchCase="'8:00 am'">(Early Start)</span>
        <span *ngSwitchCase="'10:00 am'">(Late Start)</span>
        <span *ngSwitchDefault>(Normal Start)</span>
      </div>
      <div><strong>{{ event?.price | currency:'USD' }}</strong> per/user</div>
      <div *ngIf="event?.location">
        <span>{{event?.location?.address}}</span>
        <span class="pad-left">{{event?.location?.city}}, {{event?.location?.country}}</span>
      </div>
      <div *ngIf="event?.onlineUrl">
        {{event?.onlineUrl}}
      </div>
    </div>
  `,
  styles: [`
    .thumbnail { 
      background-color: #485563;
      min-height: 210px; 
    }
    .pad-left { 
      margin-left: 10px; 
    }
    .well div { 
      color: #bbb; 
    }
  `]
})
export class EventThumbnailComponent {
  @Input() event: IEvent;

  getStartTimeStyle():any {
    if (this.event && this.event.time === '8:00 am')
      return {color: '#003300', 'font-weight': 'bold'}
    return {}
  }
} 