import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Observable } from 'rxjs';

import { EventService } from '../shared/event.service';
import { IEvent } from '../shared';

@Injectable()
export class EventListResolverService implements Resolve<any>{

  constructor(private evService: EventService) { }

  resolve(): Observable<IEvent[]> {
    return this.evService.getEvents();
  }
}
