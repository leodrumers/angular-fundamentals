import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { Observable } from 'rxjs';

import { EventService } from '../shared/event.service';
import { IEvent } from '../shared';

@Injectable()
export class EventResolverService implements Resolve<any>{

  constructor(private evService: EventService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<IEvent> {
    return this.evService.getEvent(route.params['id']);
  }
}
