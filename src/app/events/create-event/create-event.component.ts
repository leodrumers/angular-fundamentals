import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { IEvent } from '../shared/event.interface';
import { EventService } from '../shared/event.service';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styles: [`
    em {
      float: right;
      color: #E05C65;
      padding-left: 10px;
    }

    .error input { background-color: #E3C3C5; }
    .error ::-webkit-input-placeholder { color: #999; }
    .error ::-moz-placeholder { color: #999; }
    .error :-moz-placeholder { color: #999; }
    .error :ms-input-placeholder { color: #999; }
  `]
})
export class CreateEventComponent {

  isDirty   = true;
  newEvent  : IEvent;

  constructor(private router: Router, private evService: EventService) { }


  cancel(): void{
    this.router.navigate(['/events']);
  }

  saveEvent(formValues: any) : void{
    console.log(formValues);
    
    this.evService.createEvent(formValues).subscribe(
      _ => {
        this.isDirty = false;
        this.router.navigate(['events']);
      }
    );
  }
}
