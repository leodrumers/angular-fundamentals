import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core'
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { IEvent, ISession } from 'src/app/events/shared/event.interface';

@Injectable()
export class EventService {

  constructor(private http: HttpClient){}
  
  getEvents() : Observable<IEvent[]> {
    return this.http.get<IEvent[]>('/api/events')
    .pipe(
      catchError(this.handleError<IEvent[]>('getEvents', []))
    );
  }

  getEvent(id: number) : Observable<IEvent> {
    return this.http.get<IEvent>(`/api/events/${ id }`)
      .pipe(catchError(this.handleError<IEvent>('getEvent')));
  }

  createEvent(newEvent: IEvent) :Observable<IEvent> {
    return this.http.post<IEvent>('api/events', newEvent)
      .pipe(catchError(this.handleError<IEvent>('createEvent')));

  }

  searhSessions(searchTerm: string): Observable<ISession[]> {
    return this.http.get<ISession[]>(`/api/sessions/search?search=${ searchTerm }`)
    .pipe(
      catchError(this.handleError<ISession[]>('searchSessions', []))
    );
  }

  private handleError<T> (operation = 'operation', result?: T){
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }

}
