import { FormControl } from "@angular/forms";

export function restrictedWords(words){
    return (control: FormControl) : {[key: string]: any} =>{
      if(!words) return null;

      const invalidWords: string[] = words
          .map( (w: string) => control.value.includes(w) ? w : null)
          .filter( (w:string) => w != null);

      return invalidWords && invalidWords.length > 0 
            ? { 'restrictedWords': invalidWords.join(', ') } 
            : null;
    }
}