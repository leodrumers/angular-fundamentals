import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';

import { 
  ISession,
  restrictedWords
} from '../shared/index';

@Component({
  selector: 'create-session',
  templateUrl: './create-session.component.html',
  styles: [`
  em {
      float: right;
      color: #E05C65;
      padding-left: 10px;
    }

    .error input, .error select, .error textarea { background-color: #E3C3C5; }
    .error ::-webkit-input-placeholder { color: #999; }
    .error ::-moz-placeholder { color: #999; }
    .error :-moz-placeholder { color: #999; }
    .error :ms-input-placeholder { color: #999; }`]
})
export class CreateSessionComponent implements OnInit {

  @Output() saveNewSession = new EventEmitter<ISession>();
  @Output() cancelAddSession = new EventEmitter();

  sessionForm : FormGroup;

  name      : FormControl;
  presenter : FormControl;
  duration  : FormControl;
  level     : FormControl;
  abstract  : FormControl;

  ngOnInit(): void {
    this.name = new FormControl('', Validators.required );
    this.presenter = new FormControl('', Validators.required );
    this.duration = new FormControl('', Validators.required );
    this.level = new FormControl('', Validators.required );
    this.abstract = new FormControl('', [ Validators.required, Validators.maxLength(400), restrictedWords(['palabra1', 'palabra2'])] );

    this.sessionForm = new FormGroup({
      name: this.name,
      presenter: this.presenter,
      duration: this.duration,
      level: this.level,
      abstract: this.abstract,
    });
  }

  save(sessionValues: any) : void{
    const newSession : ISession = {
      id: 999,
      name: sessionValues.name,
      presenter: sessionValues.presenter,
      duration: sessionValues.duration,
      level: sessionValues.level,
      abstract: sessionValues.abstract,
      voters: []
    };

    this.saveNewSession.emit(newSession);

  }

  cancel() : void{
    this.cancelAddSession.emit();
  }
}
