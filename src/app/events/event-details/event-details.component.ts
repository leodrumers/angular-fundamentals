import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { IEvent, ISession } from '..';
import { EventService } from '../shared/event.service';



@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styles: [`
    .event-image {
      height: 100px;
    }
    a {
      cursor:pointer;
      color: green;
    }
    .filters {
      display: flex;
      justify-content: space-around;
    }

    .headers {
      margin: 10px;
    }
  `]
})
export class EventDetailsComponent implements OnInit {

  event   : IEvent;
  addMode  = false;
  filterBy = 'all';
  sortBy   = 'votes';

  constructor(private evService: EventService, private route: ActivatedRoute){}

  ngOnInit(): void {
    this.route.data.forEach( (data: Params) => {

      this.event = data['event'];
      this.addMode = false;

    });
  }

  addSession() : void {
    this.addMode = true;
  }

  saveNewSession(newSession: ISession): void{
    const nextId = Math.max.apply(null, this.event.sessions.map( s => s.id ));

    newSession.id += nextId;

    this.event.sessions.push(newSession);

    this.evService.createEvent(this.event).subscribe();
    this.addMode = false;
  }

  cancelAddSessions(): void{
    this.addMode = false;
  }

}
