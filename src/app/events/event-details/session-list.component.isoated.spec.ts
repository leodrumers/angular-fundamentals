import { SessionListComponent } from './session-list.component';
import { ISession } from '../shared/event.interface';

describe('SessionListComponent', () => {
    let component         : SessionListComponent;
    let mockAuthService :any, mockVoterService: any;

    beforeEach(() => {
        component = new SessionListComponent(mockAuthService, mockVoterService);
    })

    describe('ngOnchanges', () => {
        it('should filter the sessions correctly', () => {
            component.sessions = <ISession[]>[
                {name: 'session 4', level: 'intermediate'},
                {name: 'session 2', level: 'beginner'},
                {name: 'session 3', level: 'intermediate'},
            ];

            component.filterBy = 'intermediate';
            component.sortBy = 'name';
            component.eventId = 3;

            component.ngOnChanges();

            expect(component.visibleSessions.length).toBe(2);
        })

        it('should sort the sessions correctly', () => {
            component.sessions = <ISession[]>[
                {name: 'session 4', level: 'intermediate'},
                {name: 'session 5', level: 'beginner'},
                {name: 'session 3', level: 'intermediate'},
                {name: 'session 2', level: 'intermediate'},
            ];

            component.filterBy = 'all';
            component.sortBy = 'name';
            component.eventId = 3;

            component.ngOnChanges();

            expect(component.visibleSessions[3].name).toBe('session 5');
        })
    })
})
