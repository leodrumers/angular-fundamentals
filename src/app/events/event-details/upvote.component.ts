import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
    selector: 'upvote',
    templateUrl: './upvote.component.html',
    styleUrls: ["./upvote.component.css"]
})
export class UpVoteComponent {

    @Input() count: number;
    @Output() vote = new EventEmitter();
    
    @Input() set voted(value: boolean){
        this.iconColor = value ? 'red': 'white'
    }
    
    iconColor: string;

    onClick(){
        this.vote.emit({});
    }
}