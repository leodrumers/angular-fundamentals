import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { ISession } from '../shared/event.interface';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class VoterService {

    constructor(private http: HttpClient){}

    userHasVoted(session: ISession, voterName: string) : boolean{
        return session.voters.some( voter => voter === voterName);
    }
    
    addVoter(eventId: number, session: ISession, voterName: string) : void {
        session.voters.push(voterName);

        const url = `/api/events/${ eventId }/sessions/${ session.id }/voters/${ voterName }`;

        this.http.post(url, {}).pipe(catchError(this.handleError('addVoter'))).subscribe();
    }

    deleteVoter(eventId: number, session: ISession, voterName: string) : void{
        session.voters = session.voters.filter(voter => voter !== voterName);

        const url = `/api/events/${ eventId }/sessions/${ session.id }/voters/${ voterName }`;

        this.http.delete(url).pipe(
            catchError(this.handleError('addVoter'))).subscribe();
    }

    private handleError<T> (operation = 'operation', result?: T){
        return (error: any): Observable<T> => {
          console.error(operation + ": " + error);
          return of(result as T);
        }
    }

}
