import { Component, Input, OnChanges } from '@angular/core';

import { ISession } from '../shared/event.interface';

import { AuthService } from '../../user/services/auth.service';
import { VoterService } from './voter.service';

@Component({
  selector: 'session-list',
  templateUrl: './session-list.component.html',
  styles: [`
    .row {
      background-color: #4f5e6e;
      margin: 10px;
      padding: 1em;
    }
  `]
})
export class SessionListComponent implements OnChanges {

  @Input() sessions : ISession[];
  @Input() filterBy : string;
  @Input() sortBy   : string;
  @Input() eventId  : number;

  visibleSessions: ISession[] = [];

  constructor(public auth: AuthService, private voterService: VoterService) { }


  ngOnChanges() {
    if(this.sessions){
      this.filterSession(this.filterBy);
      this.sortBy === 'name' 
        ? this.visibleSessions.sort(sortByNameAsc)
        : this.visibleSessions.sort(sortByVotesDesc)
          
    }
  }

  filterSession(filterBy: string) {
    if(filterBy == 'all'){
      this.visibleSessions = this.sessions.slice(0);
    }else{
      this.visibleSessions = this.sessions.filter(
        session => session.level.toLocaleLowerCase() === filterBy
      );
    }
  }

  userHasVoted(session: ISession){
    return this.voterService.userHasVoted(session, this.auth.currentUser.userName);
  }

  toggleVote(session: ISession) : void {
    if(this.userHasVoted(session)){
      this.voterService.deleteVoter(this.eventId, session, this.auth.currentUser.userName );
    }else{
      this.voterService.addVoter(this.eventId, session, this.auth.currentUser.userName );
    }

    if(this.sortBy === 'votes'){
      this.visibleSessions.sort(sortByVotesDesc);
    }
  }

}

function sortByNameAsc(session1: ISession, session2: ISession): number{
  if(session1.name.toLocaleLowerCase() > session2.name.toLocaleLowerCase()) return 1
  else if (session1.name.toLocaleLowerCase() === session2.name.toLocaleLowerCase()) return 0
  else return -1
}

function sortByVotesDesc(s1: ISession, s2: ISession) : number {
  return s2.voters.length - s1.voters.length;
}

