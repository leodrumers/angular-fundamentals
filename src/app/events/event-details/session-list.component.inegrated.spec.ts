import { DebugElement, NO_ERRORS_SCHEMA } from "@angular/core";
import { ComponentFixture, TestBed } from "@angular/core/testing";

import { SessionListComponent } from './session-list.component';
import { AuthService } from '../../user/services/auth.service';
import { VoterService } from './voter.service';
import { DurationPipe } from '../shared/duration.pipe';
import { By } from "@angular/platform-browser";
import { CollapsibleWellComponent } from '../../common/collapsible-well.component';
import { UpVoteComponent } from './upvote.component';

describe('SessionListComponent', () => {

    describe('initial display', () => {

        let mockAuthService,
            mockVoterService,
            fixture     : ComponentFixture<SessionListComponent>,
            componentx  : SessionListComponent,
            element     : HTMLElement,
            debugEl     : DebugElement

        beforeEach(() => {
            mockAuthService = { isAuthenticated: () => true, currentUser: { userName: 'Joe'}}
            mockVoterService = { userHasVoted: () => true };
            TestBed.configureTestingModule({
                declarations: [
                    SessionListComponent,

                    DurationPipe,
                ],
                providers:[
                    { provide: AuthService, useValue: mockAuthService },
                    { provide: VoterService, useValue: mockVoterService }
                ],
                schemas: [
                    NO_ERRORS_SCHEMA
                ]
            });

            fixture = TestBed.createComponent(SessionListComponent);
            componentx = fixture.componentInstance;
            debugEl = fixture.debugElement;
            element = fixture.nativeElement;
        })

        describe('initial display', () => {
            it('should have the correct name', () => {
                componentx.sessions = [
                    {
                        name: 'Session 1', id: 3, presenter: 'Joe', duration: 1, 
                        level: 'beginner', abstract: 'abstract', voters: ['john', 'bob']
                    }
                ];
    
                componentx.filterBy = 'all';
                componentx.sortBy = 'name';
                componentx.eventId = 4;
                componentx.ngOnChanges();
    
                fixture.detectChanges();
            
                //expect(element.querySelector('[well-title]').textContent).toContain('Session 1');
                expect(debugEl.query(By.css('[well-title]')).nativeElement.textContent).toContain('Session 1');
                
            })
        })

    })

});