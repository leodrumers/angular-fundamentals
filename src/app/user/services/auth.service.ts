import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';

import { tap, catchError } from 'rxjs/operators';

import { IUser } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
     
  currentUser: IUser;

  constructor(private http: HttpClient) { }

  loginUser( userName: string, password: string ){

    const loginInfo = { username: userName, password};

    return this.http.post('/api/login', loginInfo).pipe(
      tap( data => this.currentUser = <IUser>data['user'] )
    ).pipe(
      catchError( _ => of(false) )
    );
  }

  isAuthenticated(): boolean {
    return !!this.currentUser;
  }

  updatecurrentUser(firstName: any, lastName: any) {
    this.currentUser.firstName = firstName;
    this.currentUser.lastName = lastName;

    return this.http.put(`/api/users/${ this.currentUser.id }`, this.currentUser);
  }

  checkAuthenticationStatus() : Observable<IUser> {
    return this.http.get<IUser>('/api/currentIdentity').pipe(
      tap(data => {
          if(data instanceof Object){
            this.currentUser = <IUser>data;
          }
        }
      )
    );
  }

  logout() {
    this.currentUser = undefined;
    return this.http.post('/api/logout', {});
  }

}
