import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { TOASTR_TOKEN, Toastr } from '../../common/toastr.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styles: [`
    em {
      float: right;
      color: #E05C65;
      padding-left: 10px;
    }

    .error input { background-color: #E3C3C5; }
    .error ::-webkit-input-placeholder { color: #999; }
    .error ::-moz-placeholder { color: #999; }
    .error :-moz-placeholder { color: #999; }
    .error :ms-input-placeholder { color: #999; }
  `]
})
export class ProfileComponent implements OnInit {


  profileForm: FormGroup;
  firstName: FormControl;
  lastName: FormControl;

  constructor(
    private auth: AuthService, 
    private route: Router,
    @Inject(TOASTR_TOKEN) private toastr: Toastr
  ) { }

  ngOnInit(): void {
    this.firstName = new FormControl(
      this.auth.currentUser.firstName, 
      [Validators.required, Validators.pattern('[a-zA-Z].*')]
    );
    this.lastName = new FormControl(
      this.auth.currentUser.lastName, [Validators.required]
    );

    this.profileForm = new FormGroup({
      firstName : this.firstName,
      lastName  : this.lastName
    });

  }

  isAnInvalidControl(controlName: string) : boolean{
    return this.profileForm.controls[controlName].invalid && this.profileForm.controls[controlName].touched
  }

  saveProfile(profileValues: any){
    if(this.profileForm.valid){
      this.auth.updatecurrentUser(profileValues.firstName, profileValues.lastName).subscribe(
        _ => {
          this.toastr.success('Cahnges saved');
        }
      );
    }
  }

  logout(){
    this.auth.logout().subscribe(_ =>{
      this.route.navigate(['/user/login']);
    });
  }

  cancel(){
    this.route.navigate(['events']);
  }
}
