import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [`
    em {
      float: right;
      color: #E05C65;
      padding-left: 10px;
    }
  `]
})
export class LoginComponent {

  mouseOverLogin: boolean;
  userName        = '';
  password        = '';
  loginInvalid   = false;

  constructor(private auth: AuthService, private route: Router) { }

  login(loginForm:any){
    this.auth.loginUser(loginForm.userName, loginForm.password).subscribe(
      resp =>{
        if(!resp){
          this.loginInvalid = true;
        }else{
          this.route.navigate(['events']);
        }
      }
    );
  }

  cancel(){
    this.route.navigate(['events']);
  }

}
