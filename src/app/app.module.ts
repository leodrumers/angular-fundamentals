import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from "@angular/common/http";


import { 
  JQ_TOKEN,
  TOASTR_TOKEN,
  Toastr,
  CollapsibleWellComponent,
  SimpleModalComponent
 } from "./common/index";

import {
  CreateEventComponent,
  CreateSessionComponent,
  EventDetailsComponent,
  EventListResolverService,
  EventService,
  EventsListComponent,
  EventThumbnailComponent,
  EventResolverService,
  SessionListComponent,
  UpVoteComponent,
  LocationValidator,
} from './events/index';

import { Error404Component } from './errors/error-404.component';
import { NavBarComponent } from './nav/nav-bar.component'

import { EventsAppComponent } from './events-app.component';
import { EventRoutingModule } from './event-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DurationPipe } from './events/shared/duration.pipe';

import { ModalTriggerDirective } from './common/modal-trigger.directive';

const toastr: Toastr = window['toastr'];
const jQuery = window['$'];

@NgModule({
  imports: [
    BrowserModule,
    EventRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  declarations: [
    CollapsibleWellComponent,
    CreateEventComponent,
    CreateSessionComponent,
    EventsAppComponent,
    EventDetailsComponent,
    EventsListComponent,
    Error404Component,
    EventThumbnailComponent,
    NavBarComponent,
    SessionListComponent,
    SimpleModalComponent,
    UpVoteComponent,

    DurationPipe,
    LocationValidator,
    ModalTriggerDirective,
  ],
  providers: [
    EventService, 
    { provide: TOASTR_TOKEN, useValue: toastr},
    { provide: JQ_TOKEN, useValue: jQuery},
    EventResolverService,
    EventListResolverService,
    { provide: 'canDeactiateCreateEvent', useValue: checkDirtyState }
  ],
  bootstrap: [EventsAppComponent]
})
export class AppModule { }

export function checkDirtyState(component: CreateEventComponent) : boolean{
  if( component.isDirty ){
    return window.confirm('No ha guardado viejito. En serio, en serio va a salir?');
  }

  return true;
}
