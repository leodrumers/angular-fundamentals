import { Component } from '@angular/core'
import { AuthService } from './user/services/auth.service';

@Component({
  selector: 'events-app',
  template: `
    <nav-bar></nav-bar>
    <div class="mt-3">
      <router-outlet></router-outlet>
    </div>
  `
})
export class EventsAppComponent {
  constructor(private auth: AuthService){}

  ngOnInit(): void {
    this.auth.checkAuthenticationStatus().subscribe();
  }

}
