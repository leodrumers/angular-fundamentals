import { Component } from '@angular/core'
import { AuthService } from '../user/services/auth.service';

import { ISession } from '../events/shared/event.interface';
import { EventService } from '../events/shared/event.service';

@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: [
    "nav-bar.component.css"
  ]
})
export class NavBarComponent {

  searchTerm = '';

  foundSessions: ISession[];

  constructor(public auth: AuthService, private evService: EventService){}

  searchSessions(searchTerm: string){
    this.evService.searhSessions(searchTerm)
    .subscribe( sessions => {
      this.foundSessions = sessions;
    });
  }

}